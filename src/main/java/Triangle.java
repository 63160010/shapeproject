/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Acer
 */
public class Triangle {
    private double high;
    private double base;
    public static final double half = 0.5;
    public Triangle(double high,double base){
        this.high =high;
        this.base =base;
    }
    public double calArea(){
        return half*high*base;
    }
    public double getH(){
        return high;
    }
    public double getB(){
        return base;
    }
    public void setHB(double high,double base){
        if(high<=0 || base<=0){
            System.out.println("Error: Radius must more than Zero!!!!");
            return;
        }
        this.high=high;
        this.base=base;
    }

}
