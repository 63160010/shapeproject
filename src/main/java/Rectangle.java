/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Acer
 */
public class Rectangle {
    private double wide;
    private double l;
    public Rectangle(double wide,double l){
        this.wide =wide;
        this.l =l;
    }
    public double calArea(){
        return wide*l;
    }
    public double getW(){
        return wide;
    }
    public double getl(){
        return l;
    }
    public void setWl(double wide,double l){
        if(wide<=0 || l<=0){
            System.out.println("Error: Radius must more than Zero!!!!");
            return;
        }
        this.wide=wide;
        this.l=l;
    }
}
